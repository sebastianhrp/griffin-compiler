﻿
using System;
using System.Text;
using System.Collections.Generic;

namespace Griffin {
/*
	public class TableData{
		Type type;
		IList<Type> args;
		SymbolTable procTable;

		public TableData(Type retType, List<Type> argList ,SymbolTable table){
			type = retType;
			if (args != null)
				args = new List<Type> ();
			if (table != null)
				table = new SymbolTable ();
		}
		public TableData(Type retType){
			type = retType;
		}
	}
*/
	public class SymbolTable: IEnumerable<KeyValuePair<string, List<Type>>> {

		IDictionary<string, List<Type>> data = new SortedDictionary<string, List<Type>>();

		//-----------------------------------------------------------
		public override string ToString() {
			var sb = new StringBuilder();
			//sb.Append("Global Symbol Table\n");
			sb.Append("=========================\n");
			foreach (var entry in data) {
				var str = "";
				foreach(var val in entry.Value){
					str += "\t"+val;
				}
				sb.Append(String.Format("{0, 15}: {1, 6}\n", 
					entry.Key, 
					str));
			}
			sb.Append("=========================\n");
			return sb.ToString();
		}

		//-----------------------------------------------------------
		public List<Type> this[string key] {
			get {
				return data[key];
			}
			set {
				data[key] = value;
			}
		}

		//-----------------------------------------------------------
		public bool Contains(string key) {
			return data.ContainsKey(key);
		}

		//-----------------------------------------------------------
		public IEnumerator<KeyValuePair<string, List<Type>>> GetEnumerator() {
			return data.GetEnumerator();
		}

		//-----------------------------------------------------------
		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator() {
			throw new NotImplementedException();
		}
	}
}
