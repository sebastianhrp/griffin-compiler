﻿using System;

namespace Griffin {

	public enum Type {
		BOOL,
		INT,
		STRING,
		LIST,
		PROC,
		VOID,
		CONST,
		VAR
	}
}
