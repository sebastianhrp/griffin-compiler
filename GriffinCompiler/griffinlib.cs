/*
  Griffin runtime library.
  Copyright (C) 2014 Flores, García, Hidalgo, ITESM CEM.
*/

using System.Collections;
using System.Collections.Generic;

namespace Griffin {
    
    using System;

    public class Utils {

		public static void WrInt(int i) {
			Console.Write(i);
		}

		public static void WrStr(string s) {
			Console.Write(s);
		}
        
        public static void WrBool(bool b) {
            Console.Write(b ? "true" : "false");
        }

		public static void WrLn() {
			Console.WriteLine();
		}

		public static int RdInt() {
			while(true){
				try{
				int a=Int32.Parse(Console.ReadLine());
				return a;
				}
				catch(Exception ex){
					Console.WriteLine(ex + "- write a correct int");
				}	
			}
		}

		public static string RdStr() {
			return Console.ReadLine();	

		}

		public static string AtStr(string s, int i) {
			if (i >= s.Length) {
				throw new IndexOutOfRangeException ();
			}

			return s.Substring(i,1);
		}

		public static int LenStr(string s) {
			return s.Length;
		}

		public static int CmpStr(string s1, string s2) {
			return string.Compare (s1, s2);
		}

		public static string CatStr(string s1, string s2) {
			return string.Concat(s1, s2);
		}

		public static string IntToStr(int i) {
			return i.ToString();
		}

		public static int StrToInt(string s) {
			return int.Parse (s);
		}

		public static int LenLstInt(List<int> loi) {
			return loi.Count;
		}

		public static int LenLstStr(List<string> los) {
			return los.Count;
		}

		public static int LenLstBool(List<bool> lob) {
			return lob.Count;
		}

		public static List<int> NewLstInt(int size) {
			List<int> list = new List<int>();
			for (int i = 1; i <= size; i++){
				list.Add(0);
			}
			return list;
        }
		public static List<string> NewLstStr(int size) {
			List<string> list = new List<string>();
			for (int i = 1; i <= size; i++){
				list.Add("");
			}
			return list;
        }
		public static List<bool> NewLstBool(int size) {
			List<bool> list = new List<bool>();
			for (int i = 1; i <= size; i++){
				list.Add(false);
			}
			return list;
        }
    }
}

