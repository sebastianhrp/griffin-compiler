/*
  Griffin compiler - Specific node subclasses for the AST (Abstract 
  Syntax Tree).
  Copyright (C) 2014 Hidalgo, Flores, García.
*/

namespace Griffin {    

    class Program: Node {}

    class ConstantDeclarationList: Node {} // Ya

	class VariableDeclarationList: Node {} // Ya

	class ProcedureDeclarationList: Node {} // Ya

    class ConstantDeclaration: Node {}	// Ya

	class VariableDeclaration: Node {}	// Ya
	
	class ListIndex: Node{}			// Ya Revisar
	
	class ParameterList: Node {}	// Ya? Revisar

	class ParameterDeclarationList: Node {}	// Ya
	
	class ParameterDeclaration: Node {}		// 

	class ProcedureDeclaration: Node {}	//

    class StatementList: Node {}		//

	class Statement: Node{}

	class CallStatement: Node {}
	
	class ExpressionList: Node {}	//

    class Assign: Node {}			//
	
	class TypeN: Node{}				//

    class If: Node {}			//
	
	class ElseIf: Node {}		//

	class Else: Node {}			//
	
    class Identifier: Node {}		//

	class List: Node {}				//
	
	class Loop: Node {}				//
		
	class For: Node {}		//

	class Exit: Node {}		//

	class Return: Node {}		//
	
	class Operand : Node {}		//
	
	class LogicExpression : Node {}		//
	
	class RelationalExpression : Node {}	//
	
	class SumExpression : Node {}	//

	class MulExpression : Node {}		//
			
	class Call : Node {}			//

    class True: Node {}

    class False: Node {}
	
	class IntLiteral: Node {}
	
	class Int: Node {}
		
	class Index: Node {}
	
	class StringN: Node {}

	class StringLiteral: Node{}

    class Not: Node {}		//

	class Minus: Node {}		//

	class RelationalOperator:Node {}

	class LogicOperator:Node {}

	class SumOperator: Node {}

	class MulOperator:Node {}
}