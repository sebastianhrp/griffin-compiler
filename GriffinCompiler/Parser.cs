/*
  Griffin compiler - This class performs the syntactic analysis,
  (a.k.a. Parsing).
  Copyright (C) 2014 Sebastián Hidalgo, Alejandro García, Alejandro Flores
*/
using System;
using System.Collections.Generic;

namespace Griffin
{
	class Parser
	{
		static readonly ISet<TokenCategory> firstOfLogicOperator =
			new HashSet<TokenCategory> () {
			TokenCategory.AND,
			TokenCategory.SAND,
			TokenCategory.SOR, 
			TokenCategory.OR,
			TokenCategory.XOR
		};

		static readonly ISet<TokenCategory> firstOfRelationalOperator =
			new HashSet<TokenCategory> () {
			TokenCategory.EQUAL,
			TokenCategory.UNEQUAL,
			TokenCategory.LESS, 
			TokenCategory.LESS_EQUAL,
			TokenCategory.GREATER_EQUAL,
			TokenCategory.GREATER
		};

		static readonly ISet<TokenCategory> firstOfSumOperator =
			new HashSet<TokenCategory> () {
			TokenCategory.PLUS,
			TokenCategory.MINUS
		};

		static readonly ISet<TokenCategory> firstOfMulOperator =
			new HashSet<TokenCategory> () {
			TokenCategory.MUL,
			TokenCategory.DIV,
			TokenCategory.REM

		};
		static readonly ISet<TokenCategory> firstOfDeclaration =
            new HashSet<TokenCategory> () {
			TokenCategory.CONST,
			TokenCategory.VAR,
			TokenCategory.IDENTIFIER,
			TokenCategory.PROC
		};
		static readonly ISet<TokenCategory> firstOfStatement =
            new HashSet<TokenCategory> () {
			TokenCategory.EXIT,
			TokenCategory.IDENTIFIER,
			TokenCategory.IF,
			TokenCategory.FOR,
			TokenCategory.PROC,
			TokenCategory.LOOP,
			TokenCategory.RETURN
		};
		static readonly ISet<TokenCategory> firstOfOperator =
            new HashSet<TokenCategory> () {
			TokenCategory.AND,
			TokenCategory.DIV,
			TokenCategory.EQUAL,
			TokenCategory.GREATER,
			TokenCategory.GREATER_EQUAL,
			TokenCategory.LESS,
			TokenCategory.LESS_EQUAL,
			TokenCategory.MINUS,		
			TokenCategory.MUL,
			TokenCategory.OR,
			TokenCategory.PLUS,
			TokenCategory.REM,
			TokenCategory.SAND,
			TokenCategory.SOR,				
			TokenCategory.UNEQUAL,
			TokenCategory.XOR
		};
		static readonly ISet<TokenCategory> firstOfSimpleExpression =
            new HashSet<TokenCategory> () {
			TokenCategory.BRACKETS_OPEN,
			TokenCategory.IDENTIFIER,
			TokenCategory.INT_LITERAL,
			TokenCategory.TRUE,
			TokenCategory.FALSE,
			TokenCategory.PARENTHESIS_OPEN,
			TokenCategory.STRING_LITERAL,
			TokenCategory.CURLY_BRACE_OPEN
		};
		static readonly ISet<TokenCategory> firstOfSimpleLiteral =
            new HashSet<TokenCategory> () {			
			TokenCategory.TRUE,
			TokenCategory.FALSE,
			TokenCategory.INT_LITERAL,
			TokenCategory.STRING_LITERAL
		};
		static readonly ISet<TokenCategory> firstOfSimpleType =
            new HashSet<TokenCategory> () {			
			TokenCategory.BOOL,
			TokenCategory.INT,
			TokenCategory.STRING
		};
		IEnumerator<Token> tokenStream;

		public Parser (IEnumerator<Token> tokenStream)
		{
			this.tokenStream = tokenStream;
			this.tokenStream.MoveNext ();
		}

		public TokenCategory CurrentToken {
			get { return tokenStream.Current.Category; }
		}

		public Token Expect (TokenCategory category)
		{
			if (CurrentToken == category) {
				Token current = tokenStream.Current;
				tokenStream.MoveNext ();
				return current;
			} else {
				throw new SyntaxError (category, tokenStream.Current);                
			}
		}

		/// Main Program method
		/*
		 * ‹program› 	→ 	[ ¿const ‹constant-declaration›+ ]
				[ ¿var ‹variable-declaration›+ ]
				‹procedure-declaration›*
				¿program ‹statement›* ¿end ¿;
		*/
		
		//AST OK
		/*--------- PROGRAM ----------*/
		public Node Program ()
		{            
			var constDeclList = new ConstantDeclarationList ();
			var varDeclList = new VariableDeclarationList ();
			var procDeclList = new ProcedureDeclarationList ();
			var stmtList = new StatementList ();

			while (firstOfDeclaration.Contains(CurrentToken)) {
				//Declaration of const, var and procedures before program
				switch (CurrentToken) {
				case TokenCategory.CONST:
					Expect (TokenCategory.CONST);
					while (CurrentToken == TokenCategory.IDENTIFIER) {
						constDeclList.Add (ConstantDeclaration ());
					}
					break;
				case TokenCategory.VAR:
					Expect (TokenCategory.VAR);
					while (CurrentToken == TokenCategory.IDENTIFIER) {
						varDeclList.Add (VariableDeclaration ());
					}
					break;
				case TokenCategory.PROC:
					while (firstOfStatement.Contains(CurrentToken)) {
						procDeclList.Add (ProcedureDeclaration ());
					}
					break;
				default:
					throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
				}
			}

			//Declaration of program
			Expect (TokenCategory.PROGRAM);
			while (firstOfStatement.Contains(CurrentToken)) {
				//Declaration of statements in program
				stmtList.Add (Statement ());
			}
			Expect (TokenCategory.END);
			Expect (TokenCategory.SEMICOLON);

			Expect (TokenCategory.EOF);

			return new Program () {
				constDeclList, 
				varDeclList, 
				procDeclList, 
				stmtList
			};
		}
		//‹constant-declaration› 	→ 	‹identifier› ¿:= ‹literal› ¿;
		//AST OK
		/*--------- CONSTANT DECLARATION ----------*/
		public Node ConstantDeclaration ()
		{
			
			var idToken = Expect (TokenCategory.IDENTIFIER);
			Expect (TokenCategory.ASSIGN);
			var literal = Literal ();
			var result = new ConstantDeclaration () { literal };
			result.AnchorToken = idToken;
			Expect (TokenCategory.SEMICOLON);
			return result;

		}
		//‹variable-declaration› 	→ 	‹identifier› ( ¿, ‹identifier› )* ¿: ‹type› ¿;
		//AST OK
		/*--------- VARIABLE DECLARATION ----------*/
		public Node VariableDeclaration ()
		{
			var result = new VariableDeclaration ();
			
			result.Add (new Identifier () {
				AnchorToken = Expect(TokenCategory.IDENTIFIER)
			});
			while (CurrentToken == TokenCategory.COMMA) {
				Expect (TokenCategory.COMMA);
				result.Add (new Identifier () {
					AnchorToken = Expect(TokenCategory.IDENTIFIER)
				});
			}

			Expect (TokenCategory.COLON);
			
			 if(CurrentToken==TokenCategory.LIST){
				result.AnchorToken = Expect (TokenCategory.LIST);
				Expect (TokenCategory.OF);
				result[0].Add(ListType());
			 }else{
				result.AnchorToken = Type ();
			}

			Expect (TokenCategory.SEMICOLON);
			
			return result;
			
		}
		//‹literal› 	→ 	‹simple-literal› | ‹list›
		//AST OK
		/*--------- LITERAL ----------*/
		public Node Literal ()
		{
			if (firstOfSimpleLiteral.Contains (CurrentToken)) {
				return SimpleLiteral ();
			} else if (CurrentToken == TokenCategory.CURLY_BRACE_OPEN) {
				return List ();
			} else {
				throw new SyntaxError (firstOfSimpleLiteral, tokenStream.Current);
			}
		}
		//‹simple-literal› 	→ 	‹integer-literal› | ‹string-literal› | ‹boolean-literal›
		//AST OK
		/*--------- SIMPLE LITERAL ----------*/
		public Node SimpleLiteral ()
		{
			switch (CurrentToken) {
			case TokenCategory.TRUE:
				return new True () {
					AnchorToken = Expect (TokenCategory.TRUE)
				};
			case TokenCategory.FALSE:
				return new False () {
					AnchorToken = Expect (TokenCategory.FALSE)
				};
			case TokenCategory.INT_LITERAL:
				return new IntLiteral () {	
					AnchorToken = Expect (TokenCategory.INT_LITERAL)
				};
			case TokenCategory.STRING_LITERAL:
				return new StringLiteral () { 
					AnchorToken = Expect (TokenCategory.STRING_LITERAL)
				};
			default:
				throw new SyntaxError (firstOfSimpleLiteral, tokenStream.Current);
			}
		}
		//‹type› 	→ 	‹simple-type› | ‹list-type›
		//AST OK
		/*--------- TYPE ----------*/
		public Token Type ()
		{
			switch (CurrentToken) {
			case TokenCategory.BOOL:
				return SimpleType ();
			case TokenCategory.INT:
				return SimpleType ();
			case TokenCategory.STRING:
				return SimpleType ();
			default:
				throw new SyntaxError (firstOfSimpleType, tokenStream.Current);
			}
		}
		
		public Node ListType(){
			return new List(){
				AnchorToken=SimpleType ()
			};
		}
		//AST OK
		/*--------- SIMPLE TYPE ----------*/
		public Token SimpleType ()
		{
			switch (CurrentToken) {
			case TokenCategory.BOOL:
				return Expect (TokenCategory.BOOL);
			case TokenCategory.INT:
				return Expect (TokenCategory.INT);
			case TokenCategory.STRING:
				return Expect (TokenCategory.STRING);
			default:
				throw new SyntaxError (firstOfSimpleType, tokenStream.Current);
			}
		}

		// LIST TYPE ?? !!!!!!!





		//‹list› 	→ 	¿{ [ ‹simple-literal› ( ¿, ‹simple-literal› )* ] ¿}
		//AST OK
		/*--------- LIST ----------*/
		public Node List ()
		{
			Expect (TokenCategory.CURLY_BRACE_OPEN);
			var varList = new List ();

			if (firstOfSimpleLiteral.Contains (CurrentToken)) {
				varList.Add (SimpleLiteral ());
				while (CurrentToken == TokenCategory.COMMA) {
					Expect (TokenCategory.COMMA);
					varList.Add (SimpleLiteral ());
				}
			}
			Expect (TokenCategory.CURLY_BRACE_CLOSE);
			return varList;
		}
		/*‹procedure-declaration› 	→ 	¿procedure ‹identifier›
								¿( ‹parameter-declaration›* ¿) [ ¿: ‹type› ] ¿;
								[ ¿const ‹constant-declaration›+ ]
								[ ¿var ‹variable-declaration›+ ]
								¿begin ‹statement›* ¿end ¿;
								*/
		//AST OK
		/*--------- PROCEDURE DECLARATION ----------*/
		public Node ProcedureDeclaration ()
		{
			Expect (TokenCategory.PROC);
			var result = new ProcedureDeclaration () {
				AnchorToken = Expect(TokenCategory.IDENTIFIER)
			};
			
			Expect (TokenCategory.PARENTHESIS_OPEN);
			var param = new ParameterDeclarationList ();
			
			while (firstOfDeclaration.Contains (CurrentToken)) {
				param.Add (ParameterDeclaration ());
			}
			Expect (TokenCategory.PARENTHESIS_CLOSE);
			
			var type = new TypeN ();
			switch (CurrentToken) {
			case TokenCategory.COLON:
				Expect (TokenCategory.COLON);
				 if(CurrentToken==TokenCategory.LIST){
					type.AnchorToken=Expect (TokenCategory.LIST);
					Expect (TokenCategory.OF);
					type[0].Add(ListType());
				 }else{
					type.AnchorToken = Type ();
				}
				
				Expect (TokenCategory.SEMICOLON);
				break;
				
			case TokenCategory.SEMICOLON:
				Expect (TokenCategory.SEMICOLON);
				break;
			default:
				throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
			}
			
			var constDeclList = new ConstantDeclarationList ();
			var varDeclList = new VariableDeclarationList ();			
			var stmtList = new StatementList ();
			
			while (firstOfDeclaration.Contains(CurrentToken)) {
				switch (CurrentToken) {
				case TokenCategory.CONST:
					Expect (TokenCategory.CONST);
					while (CurrentToken == TokenCategory.IDENTIFIER) {
						constDeclList.Add (ConstantDeclaration ());
					}
					
					break;
				case TokenCategory.VAR:
					Expect (TokenCategory.VAR);
					while (CurrentToken == TokenCategory.IDENTIFIER) {
						varDeclList.Add (VariableDeclaration ());
					}
					break;
				default:
					throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
				}
			}
				
			Expect (TokenCategory.BEGIN);
			while (firstOfStatement.Contains(CurrentToken)) {
				stmtList.Add (Statement ());
			}
			Expect (TokenCategory.END);
			Expect (TokenCategory.SEMICOLON);
			
			result.Add (param);
			result.Add (type);
			result.Add (constDeclList);
			result.Add (varDeclList);
			result.Add (stmtList);
			
			return result;

		}

		//AST OK
		/*--------- PARAMETER DECLARATION ----------*/
		public Node ParameterDeclaration ()
		{
			var param = new ParameterDeclaration ();
			param.Add (new Identifier () {
				AnchorToken = Expect(TokenCategory.IDENTIFIER)
			});
			while (CurrentToken == TokenCategory.COMMA) {
				Expect (TokenCategory.COMMA);
				param.Add (new Identifier () {
					AnchorToken = Expect(TokenCategory.IDENTIFIER)
				});			
			}
			Expect (TokenCategory.COLON);
			if(CurrentToken==TokenCategory.LIST){
				param.AnchorToken = Expect (TokenCategory.LIST);
				Expect (TokenCategory.OF);
				param[0].Add(ListType());
			 }else{
				param.AnchorToken = Type ();
			}
			Expect (TokenCategory.SEMICOLON);
			return param;
		}

		//AST OK
		/*--------- STATEMENT ----------*/
		public Node Statement ()
		{
			while (firstOfStatement.Contains (CurrentToken)) {
				switch (CurrentToken) {
				case TokenCategory.IF:
					return StateIF ();
				case TokenCategory.LOOP:
					return StateLOOP ();
				case TokenCategory.FOR:
					return StateFOR ();
				case TokenCategory.EXIT:
					return StateEXIT ();
				case TokenCategory.IDENTIFIER:
					var ident = new Identifier () {
						AnchorToken = Expect(TokenCategory.IDENTIFIER)
					};

					// Call-Statement OR Assignment-Statement
					switch (CurrentToken) {

					// Call-Satement
					/* ‹call-statement› 	→ 	‹identifier› ¿( [ ‹expression› ( ¿, ‹expression› )* ] ¿) ¿; */
					case TokenCategory.PARENTHESIS_OPEN:
						//call
						var call = new Call () {
							AnchorToken = ident.AnchorToken
						};
						var paramList = new ParameterList ();
						Expect (TokenCategory.PARENTHESIS_OPEN);

						if (firstOfSimpleExpression.Contains (CurrentToken)) {
							paramList.Add (Expression ());
							while (CurrentToken == TokenCategory.COMMA) {
								Expect (TokenCategory.COMMA);
								paramList.Add (Expression ());
							}
						}
						call.Add (paramList);
	
						Expect (TokenCategory.PARENTHESIS_CLOSE);
						Expect (TokenCategory.SEMICOLON);
						return call;

					// Assignment-Satement
					/* ‹assignment-statement› 	→ 	‹identifier› [ ¿[ ‹expression› ¿] ] ¿:= ‹expression› ¿; */
					case TokenCategory.BRACKETS_OPEN:
							//access list
						Expect (TokenCategory.BRACKETS_OPEN);
						var listIndex = new ListIndex () {
							AnchorToken = ident.AnchorToken
						};
						listIndex.Add(Expression ());
						Expect (TokenCategory.BRACKETS_CLOSE);
						Expect (TokenCategory.ASSIGN);
						var assign = new Assign () {
							AnchorToken = ident.AnchorToken
						};
						assign.Add (listIndex);
						assign.Add (Expression ());
							
						Expect (TokenCategory.SEMICOLON);
						return assign;
					case TokenCategory.ASSIGN:
							//assign
						Expect (TokenCategory.ASSIGN);
						var assign2 = new Assign () {
							AnchorToken = ident.AnchorToken
						};
						assign2.Add (Expression ());
						Expect (TokenCategory.SEMICOLON);
						return assign2;
					default:
						throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
					}
					
					
				case TokenCategory.RETURN:
					return StateRET ();
				default:
					throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
					
				}
				
			}
			return new StatementList ();
		}


		// Al correr el palindrome.griffin se "corta" en el call y asignment statements ------------------------ OJO


		/*‹if-statement›    →   ¿if ‹expression› ¿then ‹statement›*
                    ( ¿elseif ‹expression› ¿then ‹statement›* )*
                    [ ¿else ‹statement›* ] ¿end ¿;
                    */
		//AST OK
		/*--------- IF STATEMENT ----------*/
		public Node StateIF ()
		{
			var result = new If () {
				AnchorToken = Expect(TokenCategory.IF)
			};
			result.Add (Expression ());
			Expect (TokenCategory.THEN);
			var stmtList = new StatementList ();
			while (firstOfStatement.Contains(CurrentToken)) {
				stmtList.Add (Statement ());
			}
			result.Add (stmtList);
			switch (CurrentToken) {
			case TokenCategory.ELSEIF:
				while (CurrentToken == TokenCategory.ELSEIF) {
					var elsif = new ElseIf () {
						AnchorToken = Expect(TokenCategory.ELSEIF)
					};
					elsif.Add (Expression ());
					Expect (TokenCategory.THEN);
					var stmtList2 = new StatementList (); 
					while (firstOfStatement.Contains (CurrentToken)) {
						stmtList2.Add (Statement ());
					}
					elsif.Add (stmtList2);
					result.Add (elsif);
				}
				if(CurrentToken == TokenCategory.ELSE){
					Expect (TokenCategory.ELSE);
					var elseNodex = new Else();
					var stmtList3x = new StatementList (); 
					while (firstOfStatement.Contains(CurrentToken)) {
						stmtList3x.Add (Statement ());
					}
					elseNodex.Add(stmtList3x);
					result.Add (elseNodex);
				}
				Expect (TokenCategory.END);
				break;
			case TokenCategory.ELSE:
				Expect (TokenCategory.ELSE);
				var elseNode = new Else();
				var stmtList3 = new StatementList (); 
				while (firstOfStatement.Contains(CurrentToken)) {
					stmtList3.Add (Statement ());
				}
				elseNode.Add(stmtList3);
				result.Add (elseNode);
				Expect (TokenCategory.END);
				break;
			case TokenCategory.END:
				Expect (TokenCategory.END);
				break;
			default:
				throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
			}

			Expect (TokenCategory.SEMICOLON);
			return result;
		}

		// ‹loop-statement›    →   loop ‹statement›* end ;
		//AST OK
		/*--------- LOOP STATEMENT ----------*/
		public Node StateLOOP ()
		{
			var statementList = new StatementList ();
			var loop = new Loop () {
				AnchorToken = Expect(TokenCategory.LOOP)
			};
			while (firstOfStatement.Contains(CurrentToken)) {
				statementList.Add (Statement ());
			}        
			Expect (TokenCategory.END);
			Expect (TokenCategory.SEMICOLON);
			loop.Add (statementList);
			return loop;
		}

		/* for ‹identifier› in ‹expression› do
        ‹statement›* end ;*/
		//AST OK
		/*--------- FOR STATEMENT ----------*/
		public Node StateFOR ()
		{
			var forTok = new For () {
				AnchorToken = Expect(TokenCategory.FOR)
			};
            
			var ident = new Identifier(){

				AnchorToken = Expect(TokenCategory.IDENTIFIER)

			};


			Expect (TokenCategory.IN);
			var exp = Expression ();
			Expect (TokenCategory.DO);
			forTok.Add (ident);
			forTok.Add (exp);
			var statementList = new StatementList ();
			while (firstOfStatement.Contains(CurrentToken)) {
				statementList.Add (Statement ());
			} 
			forTok.Add(statementList);
			Expect (TokenCategory.END);
			Expect (TokenCategory.SEMICOLON);
			return forTok;
		}

		// return [ ‹expression› ] ;
		//AST OK
		/*--------- RETURN STATEMENT ----------*/
		public Node StateRET ()
		{
			var retRes = new Return {
				AnchorToken = Expect(TokenCategory.RETURN)
			};
			if (CurrentToken == TokenCategory.SEMICOLON) {
			} else if (firstOfSimpleExpression.Contains (CurrentToken)) {
				retRes.Add (Expression ());
			} else {
				throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
			}
			Expect (TokenCategory.SEMICOLON);
			return retRes;
		}

		//‹exit-statement›   →   ¿exit ¿;
		/*--------- EXIT STATEMENT ----------*/
		public Node StateEXIT ()
		{
			var exToken = Expect (TokenCategory.EXIT);
			Expect (TokenCategory.SEMICOLON);
			var exRes = new Exit {
				AnchorToken = exToken
			};
			return exRes;
		}

		//AST OK
		/*--------- EXPRESSION ----------*/
		public Node Expression ()
		{
			return LogicExpression ();
		}
		
		
		/*#####‹logic-expression› 	→ 	‹logic-expression› ‹logic-operator› ‹relational-expression› | ‹relational-expression›
       * ‹logic-expression› → ‹relational-expression› (‹logic-operator› ‹relational-expression›)*
		*/
		//AST OK

		/*--------- LOGIC EXPRESSION ----------*/		
		public Node LogicExpression () {



			var relExp = RelationalExpression ();


			while (firstOfLogicOperator.Contains(CurrentToken)) {



				 var exp2 = new LogicOperator{ AnchorToken= LogicOperator ()};
				 exp2.Add(relExp);
				 exp2.Add (RelationalExpression ());
				 relExp = exp2;
			}

			return relExp;
		}

		//AST OK
		/*--------- LOGIC OPERATOR ----------*/
		public Token LogicOperator ()
		{
			switch (CurrentToken) {
			case TokenCategory.AND:
				return Expect (TokenCategory.AND);
			case TokenCategory.SAND:
				return Expect (TokenCategory.SAND);
			case TokenCategory.SOR:
				return Expect (TokenCategory.SOR);
			case TokenCategory.XOR:
				return Expect (TokenCategory.XOR);
			case TokenCategory.OR:
				return Expect (TokenCategory.OR);
			default:
				throw new SyntaxError (firstOfDeclaration, tokenStream.Current);
			}
		}

		//#####‹relational-expression› 	→ 	‹relational-expression› ‹relational-operator› ‹sum-expression› | ‹sum-expression›
		//‹relational-expression› ->  sum-exp (rel-op sum-exp)*
		//AST OK
		/*--------- RELATIONAL EXPRESSION ----------*/
		public Node RelationalExpression () {

			var sumExp = SumExpression ();
			while (firstOfRelationalOperator.Contains(CurrentToken)) {
				 var exp2 = new RelationalOperator{ AnchorToken= RelationalOperator ()};

				 exp2.Add(sumExp);
				 exp2.Add (SumExpression ());
				 sumExp = exp2;
			}
			return sumExp;
		}

		/*--------- RELATIONAL OPERATOR ----------*/
		public Token RelationalOperator ()
		{
			switch (CurrentToken) {
			case TokenCategory.EQUAL:
				return Expect (TokenCategory.EQUAL);
			case TokenCategory.UNEQUAL:
				return Expect (TokenCategory.UNEQUAL);
			case TokenCategory.LESS_EQUAL:
				return Expect (TokenCategory.LESS_EQUAL);
			case TokenCategory.GREATER_EQUAL:
				return Expect (TokenCategory.GREATER_EQUAL);
			case  TokenCategory.LESS:
				return Expect (TokenCategory.LESS);
			case  TokenCategory.GREATER:
				return Expect (TokenCategory.GREATER);
			default:
				throw new SyntaxError (firstOfOperator, 
				                                  tokenStream.Current);
			}
		}
		//#####‹sum-expression› 	→ 	‹sum-expression› ‹sum-operator› ‹mul-expression› | ‹mul-expression›
		// sum-exp -> mul-exp (sum-op mul-exp)*
		/*--------- SUM EXPRESSION ----------*/

		public Node SumExpression () {
			var mulExp = MulExpression ();


			while (firstOfSumOperator.Contains(CurrentToken)) {


				 var exp2 = new SumOperator{ AnchorToken= SumOperator ()};
				 exp2.Add(mulExp);
				 exp2.Add (MulExpression ());
				 mulExp = exp2;
			}
			return mulExp;
		}

		/*--------- SUM OPERATOR ----------*/
		public Token SumOperator ()
		{
			switch (CurrentToken) {
			case TokenCategory.MINUS:
				return Expect (TokenCategory.MINUS);
			case TokenCategory.PLUS:
				return Expect (TokenCategory.PLUS);
			default:
				throw new SyntaxError (firstOfOperator, 
				                                  tokenStream.Current);
			}

		}

		//######‹mul-expression› 	→ 	‹mul-expression› ‹mul-operator› ‹unary-expression› | ‹unary-expression›
		/*--------- MUL EXPRESSION ----------*/

		public Node MulExpression () {
			var unaryExp = UnaryExpression ();



			while (firstOfMulOperator.Contains(CurrentToken)) {


				 var exp2 = new MulOperator{ AnchorToken= MulOperator ()};
				 exp2.Add(unaryExp);
				 exp2.Add (UnaryExpression ());
				 unaryExp = exp2;
			}
			return unaryExp;
		}

		/*--------- MUL OPERATOR ----------*/
		public Token MulOperator ()	{

			switch (CurrentToken) {

			case TokenCategory.MUL:
				return Expect (TokenCategory.MUL);
			case TokenCategory.DIV:
				return Expect (TokenCategory.DIV);
			case TokenCategory.REM:
				return Expect (TokenCategory.REM);
			default:
				throw new SyntaxError (firstOfOperator, 
				                                  tokenStream.Current);
			}
		}
		//‹unary-expression› 	→ 	¿not ‹unary-expression› | ¿- ‹unary-expression› | ‹simple-expression›
		//AST OK
		/*--------- UNARY EXPRESSION ----------*/
		public Node UnaryExpression ()
		{
			if (CurrentToken == TokenCategory.NOT) {
				var notNode = new Not ();
				notNode.AnchorToken = Expect (TokenCategory.NOT);
				notNode.Add (UnaryExpression ());
				return notNode;
			} else if (CurrentToken == TokenCategory.MINUS) {
				var minusNode = new Minus ();
				minusNode.AnchorToken = Expect (TokenCategory.MINUS);
				minusNode.Add (UnaryExpression ());
				return minusNode;
			} else if (firstOfSimpleExpression.Contains (CurrentToken)) {
				return SimpleExpression ();
			} else {
				throw new SyntaxError (firstOfSimpleExpression, 
				                      tokenStream.Current);
			}
		}

		//‹simple-expression› 	→ 	( ¿( ‹expression› ¿) | ‹call› | ‹identifier› | ‹literal› )[ ¿[ ‹expression› ¿] ]
		/*--------- SIMPLE EXPRESSION ----------*/
		public Node SimpleExpression (){
			if (CurrentToken == TokenCategory.PARENTHESIS_OPEN) {
				Expect (TokenCategory.PARENTHESIS_OPEN);
				var exp = Expression ();
				Expect (TokenCategory.PARENTHESIS_CLOSE);
				if (CurrentToken == TokenCategory.BRACKETS_OPEN) {
					Expect (TokenCategory.BRACKETS_OPEN);
					var exp2 = Expression ();
					Expect (TokenCategory.BRACKETS_CLOSE);
					exp.Add(exp2);
				}
				return exp;
			} else if (CurrentToken == TokenCategory.IDENTIFIER) {
				var id = Expect (TokenCategory.IDENTIFIER);
				if (CurrentToken == TokenCategory.PARENTHESIS_OPEN) {
					var callNode = new Call ();
					callNode.AnchorToken = id;
					callNode.Add (Call ());
					if (CurrentToken == TokenCategory.BRACKETS_OPEN) {
						Expect (TokenCategory.BRACKETS_OPEN);
						var exp2 = Expression ();
						Expect (TokenCategory.BRACKETS_CLOSE);
						callNode.Add(exp2);
					}
					return callNode;
				
				
				} else {
					var idNode = new Operand ();
					idNode.AnchorToken = id;
					if (CurrentToken == TokenCategory.BRACKETS_OPEN) {
						Expect (TokenCategory.BRACKETS_OPEN);
						var indNode = new Index();
						indNode.AnchorToken = id;
						var exp2 = Expression ();
						Expect (TokenCategory.BRACKETS_CLOSE);
						indNode.Add(exp2);
						return indNode;
					}
					return idNode;
				}
			} else if (CurrentToken == TokenCategory.CURLY_BRACE_OPEN || CurrentToken == TokenCategory.TRUE || CurrentToken == TokenCategory.FALSE || 
				CurrentToken == TokenCategory.INT_LITERAL || CurrentToken == TokenCategory.STRING_LITERAL) {
				switch (CurrentToken) {
				case TokenCategory.TRUE:
					var trueNode = new True ();
					trueNode.AnchorToken = Expect (TokenCategory.TRUE);
					return trueNode;
				case TokenCategory.FALSE:
					var falseNode = new False ();
					falseNode.AnchorToken = Expect (TokenCategory.FALSE);
					return falseNode;
				case TokenCategory.INT_LITERAL:
					var intNode = new Int ();
					intNode.AnchorToken = Expect (TokenCategory.INT_LITERAL);
					return intNode;
				case TokenCategory.STRING_LITERAL:
					var stringNode = new StringN ();
					stringNode.AnchorToken = Expect (TokenCategory.STRING_LITERAL);
					return stringNode;
				case TokenCategory.CURLY_BRACE_OPEN:
					return List ();
				default:throw new SyntaxError (firstOfOperator, 
				                      tokenStream.Current);	
				}
			} else {
				throw new SyntaxError (firstOfOperator, 
				                      tokenStream.Current);
			}
			
		}

		/*--------- CALL ----------*/
		public Node Call ()
		{
			var expList = new ExpressionList ();
			Expect (TokenCategory.PARENTHESIS_OPEN);
			if (firstOfSimpleExpression.Contains (CurrentToken)) {
				expList.Add (Expression ());
				while (CurrentToken == TokenCategory.COMMA) {
					Expect (TokenCategory.COMMA);
					expList.Add (Expression ());
				}
			}
            
			Expect (TokenCategory.PARENTHESIS_CLOSE);
			return expList;
		}
	}
}
