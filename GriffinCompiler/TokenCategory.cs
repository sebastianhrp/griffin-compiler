/*
  Griffin compiler - Token categories for the scanner.
  Copyright (C) 2014 Sebastián Hidalgo, Alejandro García, Alejandro Flores
*/  


namespace Griffin {

    enum TokenCategory {
		AND,					//and
		ASSIGN,					//:=
		BEGIN,					//begin
		BLOCK_COMMENT_LEFT,		//(*
		BLOCK_COMMENT_RIGHT,	//*)
		BOOL,					//bool
		BRACKETS_OPEN,			//[
		BRACKETS_CLOSE,			//]
		CONST,					//const
		COLON,					//:
		COMMA,					//,
		CURLY_BRACE_OPEN,		//{
		CURLY_BRACE_CLOSE,		//}
		DIV,					//div
		DO,						//do
		ELSEIF,					//elseif
		ELSE,					//else
		END,					//end
		EOF,
		EQUAL,					//=
		EXIT,					//exit
		FALSE,					//false
		FOR,					// for
		GREATER,				//>
		GREATER_EQUAL,			//>=
		IDENTIFIER,				//A-Z,0-9
		IF,						//if
		IN,						//in
		INT,					//int
		INT_LITERAL,			//0-9
		LESS,					//<
		LESS_EQUAL,				//<=			
		LINE_COMMENT, 			//--
		LIST,					//list of
		LOOP,					//loop
		MINUS,					//-
		MUL,					//*
		NOT,					//not
		OF,						//of
		OR,						//or
		PARENTHESIS_OPEN,		//(
		PARENTHESIS_CLOSE,		//)
		PLUS,					//+
		PROCEDURE,				//procedure
		PROC,					//proc
		PROGRAM,				//program
		RETURN,					//return
		REM,					//rem
		SAND,					//sand
		SEMICOLON,				//;
		SOR,					//sor
		STRING,					//string
		STRING_LITERAL,			//a-Z*
		THEN,					//then
		TRUE,					//true
		UNEQUAL,				//<>
		VAR,					//var
		XOR,					//xor
		ILLEGAL_CHAR
    }
}

