﻿/*
  Griffin compiler - Program driver.
  Copyright (C) 2014 Sebastián Hidalgo, Alejandro García, Alejandro Flores
  
*/  


using System;
using System.IO;
using System.Text;

namespace Griffin {

	public class Driver {

		const string VERSION = "0.5";

		//-----------------------------------------------------------
		static readonly string[] ReleaseIncludes = {
			"Lexical analysis",
			"Syntactic analysis",
			"AST construction",
			"Semantic analysis",
			"CIL code generation"
		};

		//-----------------------------------------------------------
		void PrintAppHeader() {
			Console.WriteLine("Griffin compiler, version " + VERSION);
			Console.WriteLine("Copyright \u00A9 2014 by Flores, Garcia, Hidalgo, ITESM CEM."                
			);
			Console.WriteLine("This program is free software; you may "
			 + "redistribute it under the terms of the GNU General Public License version 3 or later.");
			Console.WriteLine("This program has absolutely no warranty.");
		}

		//-----------------------------------------------------------
		void PrintReleaseIncludes() {
			Console.WriteLine("Included in this release:");            
			foreach (var phase in ReleaseIncludes) {
				Console.WriteLine("   * " + phase);
			}
		}


		//-----------------------------------------------------------
		void Run(string[] args) {
			
			PrintAppHeader();
			Console.WriteLine();
			PrintReleaseIncludes();
			Console.WriteLine();

			if (args.Length != 2) {
				Console.Error.WriteLine(
					"Please specify the name of the input file.");
				Environment.Exit(1);
			}

			try {            
				var inputPath = args[0];
				var outputPath = args[1];                
				var input = File.ReadAllText(inputPath);
				var parser = new Parser(new Scanner(input).Start().GetEnumerator());
				var program = parser.Program();
				Console.Write(program.ToStringTree());

				var semantic = new SemanticAnalyzer();
                semantic.Visit((dynamic) program);
                Console.WriteLine("Semantics OK.");
                Console.WriteLine("Global Table");
				var tb = semantic.Tables["Table"].ToString();
				Console.WriteLine(tb);
				Console.WriteLine("Const Table");
				var ctb = semantic.Tables["TableConst"].ToString();
				Console.WriteLine(ctb);

				Console.WriteLine("Proc Tables");
				foreach(var table in semantic.Tables) {
					if (table.Key == "Table" || table.Key == "TableConst") {

					} else {
						Console.WriteLine("Table name: " + table.Key);
						Console.WriteLine(table.Value);
					}
				}


				var codeGenerator = new CILGenerator(semantic.Tables);
                File.WriteAllText(
                    outputPath,
                    codeGenerator.Visit((dynamic) program));
                Console.WriteLine(
                    "Generated CIL code to '" + outputPath + "'.");
                Console.WriteLine();

			} catch (Exception e) {

				if (e is FileNotFoundException || e is SyntaxError) {
					Console.Error.WriteLine(e.Message);
					Environment.Exit(1);
				}

				throw;
			}

			/*
			PrintAppHeader();
			Console.WriteLine();
			PrintReleaseIncludes();
			Console.WriteLine();

			if (args.Length != 1) {
				Console.Error.WriteLine(
					"Please specify the name of the input file.");
				Environment.Exit(1);
			}

			try {            
				var inputPath = args[0];                
				var input = File.ReadAllText(inputPath);
				var parser = new Parser(new Scanner(input).Start().GetEnumerator());


				Console.WriteLine(String.Format(
					"===== Tokens from: \"{0}\" =====", inputPath)
				);
				var count = 1;
				
				foreach (var tok in new Scanner(input).Start()) {
					Console.WriteLine(String.Format("[{0}] {1}", 	count++, tok));
				}

				parser.Program();

				Console.WriteLine("\nSyntax OK\n");

				var program = parser.Program();
                Console.Write(program.ToStringTree());

			} catch (Exception e) {
				if (e is FileNotFoundException || e is SyntaxError) {
                    Console.Error.WriteLine(e.Message);
                    Environment.Exit(1);
                }

                throw;
			}   */             
		}



		//-----------------------------------------------------------
		public static void Main(string[] args) {
			new Driver().Run(args);
		}

	}
}

