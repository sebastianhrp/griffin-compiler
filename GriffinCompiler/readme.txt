﻿Griffin compiler, version 0.1
===============================

This program is free software. You may redistribute it under the terms of
the GNU General Public License version 3 or later. See license.txt for 
details.    

Included in this release:

   * Lexical analysis
    
To build, at the terminal type:

    make
   
To run, type:

    ./griffin.exe <file_name>
    
Where <file_name> is the name of a Griffin source file. You can try with
these files:

   * hello.griffin
   * palindrome.griffin

