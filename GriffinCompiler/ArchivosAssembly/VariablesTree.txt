
Program
  ConstantDeclarationList
    ConstantDeclaration {IDENTIFIER, "konstant", @(7, 5)}
      IntLiteral {INT_LITERAL, "1", @(7, 17)}
  VariableDeclarationList
    VariableDeclaration {INT, "integer", @(10, 11)}
      Identifier {IDENTIFIER, "x", @(10, 5)}
      Identifier {IDENTIFIER, "y", @(10, 8)}
  ProcedureDeclarationList
    ProcedureDeclaration {IDENTIFIER, "F", @(12, 11)}
      ParameterDeclarationList
        ParameterDeclaration {BOOL, "boolean", @(13, 8)}
          Identifier {IDENTIFIER, "b", @(13, 5)}
        ParameterDeclaration {INT, "integer", @(14, 8)}
          Identifier {IDENTIFIER, "z", @(14, 5)}
      TypeN
      ConstantDeclarationList
        ConstantDeclaration {IDENTIFIER, "konstant", @(17, 9)}
          IntLiteral {INT_LITERAL, "1000", @(17, 21)}
      VariableDeclarationList
        VariableDeclaration {INT, "integer", @(19, 12)}
          Identifier {IDENTIFIER, "x", @(19, 9)}
      StatementList
        Assign {IDENTIFIER, "x", @(22, 9)}
          SumOperator {PLUS, "+", @(22, 16)}
            Operand {IDENTIFIER, "x", @(22, 14)}
            Operand {IDENTIFIER, "konstant", @(22, 18)}
        Assign {IDENTIFIER, "y", @(23, 9)}
          SumOperator {PLUS, "+", @(23, 16)}
            Operand {IDENTIFIER, "y", @(23, 14)}
            Operand {IDENTIFIER, "konstant", @(23, 18)}
        Call {IDENTIFIER, "WrBool", @(24, 3)}
          ParameterList
            Operand {IDENTIFIER, "b", @(24, 10)}
        Call {IDENTIFIER, "WrLn", @(25, 9)}
          ParameterList
        Assign {IDENTIFIER, "z", @(26, 9)}
          Minus {MINUS, "-", @(26, 14)}
            Operand {IDENTIFIER, "z", @(26, 15)}
        Call {IDENTIFIER, "WrInt", @(27, 9)}
          ParameterList
            Operand {IDENTIFIER, "z", @(27, 15)}
        Call {IDENTIFIER, "WrLn", @(28, 9)}
          ParameterList
        Call {IDENTIFIER, "WrInt", @(30, 9)}
          ParameterList
            Operand {IDENTIFIER, "x", @(30, 15)}
        Call {IDENTIFIER, "WrLn", @(31, 9)}
          ParameterList
        Call {IDENTIFIER, "WrInt", @(32, 9)}
          ParameterList
            Operand {IDENTIFIER, "y", @(32, 15)}
        Call {IDENTIFIER, "WrLn", @(33, 9)}
          ParameterList
    ProcedureDeclaration {IDENTIFIER, "G", @(36, 11)}
      ParameterDeclarationList
      TypeN
      ConstantDeclarationList
      VariableDeclarationList
        VariableDeclaration {INT, "integer", @(38, 12)}
          Identifier {IDENTIFIER, "y", @(38, 9)}
      StatementList
        Assign {IDENTIFIER, "x", @(40, 9)}
          SumOperator {PLUS, "+", @(40, 16)}
            Operand {IDENTIFIER, "x", @(40, 14)}
            Operand {IDENTIFIER, "konstant", @(40, 18)}
        Assign {IDENTIFIER, "y", @(41, 9)}
          SumOperator {PLUS, "+", @(41, 16)}
            Operand {IDENTIFIER, "y", @(41, 14)}
            Operand {IDENTIFIER, "konstant", @(41, 18)}
        Call {IDENTIFIER, "WrInt", @(42, 9)}
          ParameterList
            Operand {IDENTIFIER, "x", @(42, 15)}
        Call {IDENTIFIER, "WrLn", @(43, 9)}
          ParameterList
        Call {IDENTIFIER, "WrInt", @(44, 9)}
          ParameterList
            Operand {IDENTIFIER, "y", @(44, 15)}
        Call {IDENTIFIER, "WrLn", @(45, 9)}
          ParameterList
        Call {IDENTIFIER, "F", @(46, 9)}
          ParameterList
            False {FALSE, "false", @(46, 11)}
            Int {INT_LITERAL, "5", @(46, 18)}
  StatementList
    Call {IDENTIFIER, "G", @(50, 5)}
      ParameterList
Table for procedure: F
        [b              BOOL ]
        [konstant               INT ]
        [x              INT ]
        [z              INT ]
Table for procedure: G
        [y              INT ]
Semantics OK.
Global Table
Global Symbol Table
=========================
          AtStr:        PROC    STRING  STRING  INT
         CatStr:        PROC    STRING  STRING  STRING
         CmpStr:        PROC    INT     STRING  STRING
              F:        PROC    VOID    BOOL    INT
              G:        PROC    VOID
       IntToStr:        PROC    STRING  INT
       konstant:        INT
     LenLstBool:        PROC    INT     LIST    BOOL
      LenLstInt:        PROC    INT     LIST    INT
      LenLstStr:        PROC    INT     LIST    STRING
         LenStr:        PROC    INT     STRING
     NewLstBool:        PROC    LIST    BOOL
      NewLstInt:        PROC    LIST    INT
      NewLstStr:        PROC    LIST    STRING
          RdInt:        PROC    INT     VOID
          RdStr:        PROC    STRING  VOID
       StrToInt:        PROC    INT     STRING
         WrBool:        PROC    VOID    BOOL
          WrInt:        PROC    VOID    INT
           WrLn:        PROC    VOID
          WrStr:        PROC    VOID    STRING
              x:        INT
              y:        INT
=========================