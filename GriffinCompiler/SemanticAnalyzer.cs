﻿using System;
using System.Collections.Generic;

namespace Griffin {

	class SemanticAnalyzer {
		IDictionary<string, SymbolTable> tableSummary;
		//-----------------------------------------------------------
		static readonly IDictionary<TokenCategory, Type> typeMapper =
			new Dictionary<TokenCategory, Type> () {
			{ TokenCategory.BOOL, Type.BOOL },
			{ TokenCategory.INT_LITERAL, Type.INT },
			{ TokenCategory.INT, Type.INT },
			{ TokenCategory.STRING_LITERAL, Type.STRING },
			{ TokenCategory.STRING, Type.STRING },
			{ TokenCategory.LIST, Type.LIST },
			{ TokenCategory.PROC, Type.PROC },
			{ TokenCategory.TRUE, Type.BOOL },
			{ TokenCategory.FALSE, Type.BOOL }
		};

		//-----------------------------------------------------------
		public IDictionary<string, SymbolTable> Tables {
			get{
				return tableSummary;
			}
		}

		//-----------------------------------------------------------
		public SymbolTable Table {
			get;
			private set;
		}

		//-----------------------------------------------------------
		public SymbolTable AuxTable {
			get;
			private set;
		}
		
		//-----------------------------------------------------------
		public SymbolTable ConstTable {
			get;
			private set;
		}
		public SymbolTable AuxConstTable {
			get;
			private set;
		}
		//-----------------------------------------------------------
		public bool isProcDeclaration = false;
		public string actualProc = "";
		public Stack<bool> isLoop = new Stack<bool>();

		//-----------------------------------------------------------
		public SemanticAnalyzer () {
			Table = new SymbolTable ();
			AuxTable = new SymbolTable ();
			ConstTable = new SymbolTable ();
			AuxConstTable = new SymbolTable ();
			tableSummary = new SortedDictionary<string, SymbolTable>();
		}

		//-----------------------------------------------------------
		public Type Visit (Program node) {
			//ConstDesc
			Visit ((dynamic)node [0]);
			//VarDec
			Visit ((dynamic)node [1]);
			//Console.WriteLine("Tabla despues de variables");
			//ProcDec
			Visit ((dynamic)node [2]);
			AuxTable = new SymbolTable ();
			AuxConstTable = new SymbolTable ();

			//Console.WriteLine("Tabla despues de procs");
			//ProgStatements
			Visit ((dynamic)node [3]);
			tableSummary.Add ("Table", Table);
			tableSummary.Add ("TableConst", ConstTable);
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (ConstantDeclarationList node) {
			VisitChildren (node);
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (VariableDeclarationList node) {
			VisitChildren (node);
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (ProcedureDeclarationList node) {
			isProcDeclaration = true;
			// Add the Griffin API procedure declarations
			/* IO operations: */
			Table ["WrInt"]= new List<Type> {Type.PROC, Type.VOID, Type.INT};
			Table ["WrStr"]= new List<Type> {Type.PROC, Type.VOID, Type.STRING};
			Table ["WrBool"]= new List<Type> {Type.PROC, Type.VOID, Type.BOOL};
			Table ["WrLn"]= new List<Type> {Type.PROC, Type.VOID};
			Table ["RdInt"]= new List<Type> {Type.PROC, Type.INT, Type.VOID};
			Table ["RdStr"]= new List<Type> {Type.PROC, Type.STRING, Type.VOID};

			/* String operations: */
			Table ["AtStr"]= new List<Type> {Type.PROC, Type.STRING, Type.STRING, Type.INT};
			Table ["LenStr"]= new List<Type> {Type.PROC, Type.INT, Type.STRING};
			Table ["CmpStr"]= new List<Type> {Type.PROC, Type.INT, Type.STRING, Type.STRING};
			Table ["CatStr"]= new List<Type> {Type.PROC, Type.STRING, Type.STRING, Type.STRING};

			/* List operations: */
			Table ["LenLstInt"]= new List<Type> {Type.PROC, Type.INT, Type.LIST, Type.INT}; // List of integers
			Table ["LenLstStr"]= new List<Type> {Type.PROC, Type.INT, Type.LIST, Type.STRING}; // List of string
			Table ["LenLstBool"]= new List<Type> {Type.PROC, Type.INT, Type.LIST,Type.BOOL}; // List of boolean
			Table ["NewLstInt"]= new List<Type> {Type.PROC, Type.LIST, Type.INT}; // List of integers
			Table ["NewLstStr"]= new List<Type> {Type.PROC, Type.LIST, Type.STRING}; // List of string
			Table ["NewLstBool"]= new List<Type> {Type.PROC, Type.LIST, Type.BOOL}; // List of boolean

			/* Conversion operations: */
			Table ["IntToStr"]= new List<Type> {Type.PROC, Type.STRING, Type.INT};
			Table ["StrToInt"]= new List<Type> {Type.PROC, Type.INT, Type.STRING};

			VisitChildren (node);
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (StatementList node) {
			VisitChildren (node);
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (ConstantDeclaration node) {
			//Console.WriteLine ("Entro Constantes");

			if (isProcDeclaration) {

				var constName = node.AnchorToken.Lexeme;
				if (AuxTable.Contains (constName)) {
					throw new SemanticError (
						"Duplicated variable: " + constName,
						node [0].AnchorToken);
				} else {
					AuxTable [constName]= new List<Type>();
					AuxTable [constName].Add( typeMapper [node [0].AnchorToken.Category]);
					AuxTable [constName].Add( Type.CONST );
					AuxConstTable [constName]= new List<Type>();
					AuxConstTable [constName].Add( Type.CONST );
					AuxConstTable [constName].Add( typeMapper [node [0].AnchorToken.Category]);
					tableSummary.Add (actualProc + "Const", AuxConstTable);
					if(typeMapper[node [0].AnchorToken.Category]==Type.INT){
					
						try {
							Convert.ToInt32 (node[0].AnchorToken.Lexeme);

					} catch (OverflowException) {
						throw new SemanticError (
					"Integer literal too large: " + node [0].AnchorToken.Lexeme, 
					node.AnchorToken);
					}

					}			
				}

			} else {

				var constName = node.AnchorToken.Lexeme;
				if (Table.Contains (constName)) {
					throw new SemanticError (
						"Duplicated variable: " + constName,
						node [0].AnchorToken);

				} else {
					if(node[0] is List){
						
						//Es una lista
						
						//Revisar que no este vacia
						if(node[0].Count() == 0){

									throw new SemanticError (
								"Cant declare empty constant list " + constName,node.AnchorToken);
						}
						var type = node[0][0].AnchorToken.Category;
						//Console.WriteLine("ASDS "+type);
						if(typeMapper[type]==Type.BOOL || typeMapper[type]==Type.BOOL){
							type = TokenCategory.BOOL;
						}

						//Checa que todos en la lista sean del mismo tipo
						foreach(var n in node[0]){
							if(typeMapper [n.AnchorToken.Category] != typeMapper[type]){
									throw new SemanticError (
								"List of different types: " + constName,node.AnchorToken);
									
							}
						}

						//Agrega a tablas
						Table [constName]= new List<Type>();
						Table [constName].Add(Type.LIST);
						ConstTable [constName]= new List<Type>();
						ConstTable [constName].Add( Type.LIST);
					
						//Switch para agregar el tipo de la lista
						//Console.WriteLine("safa "+type);
						switch (type) {
						case TokenCategory.INT_LITERAL:
							Table [constName].Add(Type.INT);
							ConstTable [constName].Add( Type.INT);
							break;
						case TokenCategory.BOOL:
							Table [constName].Add(Type.BOOL);
							ConstTable [constName].Add( Type.BOOL);
							break;
						case TokenCategory.STRING_LITERAL:
							Table [constName].Add(Type.STRING);
							ConstTable [constName].Add( Type.STRING);
							break;
						default:
							throw new SemanticError (
								"List of invalid type: " + constName,node.AnchorToken);
									
						}

					}else{

						Table [constName]= new List<Type>();
						Table [constName].Add( typeMapper [node [0].AnchorToken.Category]);
						Table [constName].Add( Type.CONST);
						ConstTable [constName]= new List<Type>();
						ConstTable [constName].Add( typeMapper [node [0].AnchorToken.Category]);

						if(typeMapper[node [0].AnchorToken.Category]==Type.INT){
							try {
									Convert.ToInt32 (node[0].AnchorToken.Lexeme);
							} catch (OverflowException) {
								throw new SemanticError (
							"Integer literal too large: " + node [0].AnchorToken.Lexeme, 
							node.AnchorToken);
							}
						}
					}		
						
				}

			}
		
			//Console.WriteLine ("Terminó constantes");
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (VariableDeclaration node) {
			//Console.WriteLine("Entro variables");
			foreach (var variable in node) {

				var variableName = variable.AnchorToken.Lexeme;

				if (isProcDeclaration) {
					if (AuxTable.Contains (variableName)) {
						throw new SemanticError (
							"Duplicated variable: " + variableName,
							node [0].AnchorToken);
					} else {
						AuxTable [variableName]= new List<Type>();
						var cat =node.AnchorToken.Category;
						AuxTable [variableName].Add(typeMapper [cat]);
						AuxTable [variableName].Add(Type.VAR);
										//Console.WriteLine(	node.AnchorToken.Lexeme);
											
						if(typeMapper[cat]==Type.LIST){
							Table [variableName].Add(typeMapper[node[0][0].AnchorToken.Category]);
						}
					}

				} else {

					if (Table.Contains (variableName)) {
						throw new SemanticError (
							"Duplicated variable: " + variableName,
							node [0].AnchorToken);
					} else {


						Table [variableName]= new List<Type>();
						var cat =node.AnchorToken.Category;
						Table [variableName].Add(typeMapper [cat]);
						Table [variableName].Add(Type.VAR);
									
						if(typeMapper[cat]==Type.LIST){
								Table [variableName].Add(typeMapper[node[0][0].AnchorToken.Category]);
						}
					}
				}	


			}
			//Console.WriteLine ("Terimno Variables");


			return Type.VOID;
		}

		//----------------------------------------------------------- 
		public Type Visit (ListIndex node) {
			if (isProcDeclaration) {
				var identName = node.AnchorToken.Lexeme;
				var expType = Visit ((dynamic)node [0]);
				if(expType!=Type.INT){
					throw new SemanticError (
					"Invalid Index in ",
					node.AnchorToken);
				}
				
				if (AuxTable.Contains (identName)) {
					return AuxTable[identName][1];
				}else if(Table.Contains (identName)){
					return Table [identName][1];
				}else{
					throw new SemanticError (
					"Undeclared List: " + identName,
					node.AnchorToken);
				}
			} else {
				var identName = node.AnchorToken.Lexeme;
				var expType = Visit ((dynamic)node [0]);
				if(expType!=Type.INT){
					throw new SemanticError (
					"Invalid Index in ",
					node.AnchorToken);
				}
				
				if (AuxTable.Contains (identName)) {
					return AuxTable[identName][1];
				}else {
					throw new SemanticError (
					"Undeclared List: " + identName,
					node.AnchorToken);
				}
			}
		}

		//-----------------------------------------------------------
		public Type Visit (ParameterList node) {
			return Type.VOID;
		}

		//-----------------------------------------------------------
		public Type Visit (ParameterDeclarationList node) {
			VisitChildren (node);
			return Type.VOID;
		}

		//-----------------------------------------------------------YA
		public Type Visit (ParameterDeclaration node) {

			var variableName = node [0].AnchorToken.Lexeme;
			if (AuxTable.Contains (variableName)) {
				throw new SemanticError (
					"Duplicated variable: " + variableName,
					node [0].AnchorToken);
			} else {
				AuxTable [variableName]= new List<Type>();
				var cat = node.AnchorToken.Category;
				AuxTable [variableName].Add(typeMapper [cat]);
				Table [actualProc].Add (typeMapper [cat]);

				if(typeMapper[cat]==Type.LIST){
					AuxTable [variableName].Add(typeMapper[node[0][0].AnchorToken.Category]);
					Table [actualProc].Add(typeMapper[node[0][0].AnchorToken.Category]);
				}
				
			}	
			//Console.WriteLine ("terminé Parametro " + variableName);	
			return Type.VOID;
		}

		//--------------------------------------------------------
		public Type Visit (ProcedureDeclaration node) {
			AuxTable = new SymbolTable();
			AuxConstTable = new SymbolTable();
			var procName = node.AnchorToken.Lexeme;
			actualProc = procName;
			//Console.WriteLine ("Enter Procedure " + procName);		

			if (Table.Contains (procName)) {
				throw new SemanticError (
					"Duplicated variable: " + procName,
					node.AnchorToken);
			} else {
				Table [procName]= new List<Type>();
				Table [procName].Add( Type.PROC);
				Table [procName].Add(Visit((dynamic)node[1]));
			}		
			VisitChildren (node);
			//Console.WriteLine ("Finish Procedure " + procName);	
			Console.WriteLine ("Table for procedure: " + procName);	
			tableSummary.Add (procName, AuxTable);
			PrintTable();	
			return Type.VOID;
		}

		//--------------------------------------------------------YA
		public Type Visit (Statement node) {
			VisitChildren (node);
			return Type.VOID;
		}

		//------------------------------------------------------Revisar caso []
		public Type Visit (Assign node) {

			var variableName = node.AnchorToken.Lexeme;

			//Console.WriteLine ("Asignar " + variableName);
			
			if (ConstTable.Contains (variableName)||AuxConstTable.Contains (variableName)) {
				throw new SemanticError (
							"Constant  " + variableName +
							" can't be reassigned",
							node.AnchorToken);
			}

			if (AuxTable.Contains (variableName)) {
				var expectedType = AuxTable [variableName][0]; 
				
				if(expectedType == Type.LIST){	
					expectedType = AuxTable [variableName][1];
					if(node[0].GetType().Name =="List"){
						var listType = typeMapper[node[0][0].AnchorToken.Category];
						if(expectedType!=listType){
							throw new SemanticError (
							"Expecting type " + expectedType 
							+ " in assignment statement",
							node.AnchorToken);
						}else{
							expectedType = AuxTable [variableName][0]; 
						}
					}
				}
				if (expectedType != Visit ((dynamic)node [0])) {
					throw new SemanticError (
						"Expecting type " + expectedType 
						+ " in assignment statement",
						node.AnchorToken);
				}

			} else if (Table.Contains (variableName)) {

				var expectedType = Table [variableName][0];

				if(expectedType == Type.LIST){	
					expectedType = Table [variableName][1];
					if(node[0].GetType().Name =="List"){
						var listType = typeMapper[node[0][0].AnchorToken.Category];
						if(expectedType!=listType){
							throw new SemanticError (
							"Expecting type " + expectedType 
							+ " in assignment statement",
							node.AnchorToken);
						}else{
							expectedType = Table [variableName][0]; 
						}
					}
				}
				if (expectedType != Visit ((dynamic)node [0])) {
					throw new SemanticError (
						"Expecting type " + expectedType 
						+ " in assignment statement",
						node.AnchorToken);
				}

			} else {
				throw new SemanticError (
					"Undeclared variable: " + variableName,
					node.AnchorToken);
			}

			//Console.WriteLine ("Asignada " + variableName);

			return Type.VOID;
		}

		//------------------------------------------------------
		public Type Visit (TypeN node) {
			
			if(node.AnchorToken==null){
				return Type.VOID;
			}else{
				return typeMapper[node.AnchorToken.Category];
			}
			
		}

		//------------------------------------------------------
		public Type Visit (Operand node) { //-------------Revisar
			var variableName = node.AnchorToken.Lexeme;
			if (AuxTable.Contains (variableName)) {
				return AuxTable [variableName][0];
			} else if (Table.Contains (variableName)) {
				return Table [variableName][0];
			} else {
				throw new SemanticError (
					"Undeclared variable: " + variableName,
					node.AnchorToken);
			}
		}
		
		//------------------------------------------------------
		public Type Visit (Index node) { 
			var variableName = node.AnchorToken.Lexeme;
			var indice = Visit ((dynamic)node [0]);
			if(indice!=Type.INT){
				throw new SemanticError (
					"Invalid Index in ",
					node.AnchorToken);
			}
			if (AuxTable.Contains (variableName)) {
				return AuxTable [variableName][1];
			} else if (Table.Contains (variableName)) {
				return Table [variableName][1];
			} else {
				throw new SemanticError (
					"Undeclared variable: " + variableName,
					node.AnchorToken);
			}
		}
		
		public Type Visit(List node){
			var tipo = typeMapper[node[0].AnchorToken.Category];
			foreach(var n in node){
				if(tipo!=typeMapper[n.AnchorToken.Category]){
					throw new SemanticError (
					"Wrong type on list: " ,
					n.AnchorToken);
				}
			}
			return Type.LIST;
		}

		//------------------------------------------------------
		public Type Visit (RelationalOperator node) { 
			var op = (string)node.AnchorToken.Lexeme;

			if (op == "<>" || op == "=") {
				// The two posible cases all-BOOL or all-INT
				var case1 = Visit ((dynamic)node [0]) == Type.BOOL &&
					Visit ((dynamic)node [1]) == Type.BOOL; 
				var case2 = Visit ((dynamic)node [0]) == Type.INT &&
					Visit ((dynamic)node [1]) == Type.INT;

				// Check both cases
				if ( !(case1 || case2) ) {
					throw new SemanticError (
						String.Format (
						"Operator {0} requires two operands of type {1}",
						op, (case1 ? Type.BOOL : Type.INT)
						),
						node.AnchorToken);
				}
			}

			if (op == "<" || op == ">" || op == "<=" || op == ">=") {
				if (Visit ((dynamic)node [0]) != Type.INT &&
				    Visit ((dynamic)node [1]) != Type.INT) {
					throw new SemanticError (String.Format (
						"Operator {0} requires two operands of type {1}",
						op, Type.INT), node.AnchorToken);
				}
			}

			return Type.BOOL;
		}

		//------------------------------------------------------
		public Type Visit (Call node) {
			var procName = node.AnchorToken.Lexeme;
			if (Table.Contains (procName)) {
				int i = 2;	
				int j = 0;	

				foreach(var x in Table[procName]){
					j++;
					if(j>2){
						if(x == Type.VOID){
							j--;
						}
					}
					if(x == Type.LIST)
						j--;
				}
				j=j-2;
				
				var k =0;
				foreach(var n in node[0]){
					k++;		
				}

				//Console.WriteLine(j + " " + k);
				if(j!=k){
				throw new SemanticError(
										"Wrong number of args in" + procName 
										+ " in call statement", node.AnchorToken);
				}
				var lst = false;
				foreach(var n in node[0]){
					if(!lst){
						if(Visit((dynamic)n)==Type.LIST&&Visit((dynamic)n) == Table[procName][i]){

							if(Table[procName][i+1]==Table[n.AnchorToken.Lexeme][1])
							i+=2;
							lst=true;
							continue;
						}
						if( Visit((dynamic)n) != Table[procName][i] ) {
									throw new SemanticError(
										"Expecting type " + Table[procName][i] 
										+ " in call statement", node.AnchorToken);
						}
						i++;
					}else{
						lst=false;
					}
				}
					
				return Table[procName][1];
				
			} else {
				throw new SemanticError (
					"Undeclared procedure: " + procName,
					node.AnchorToken);
			}
			
			
		}

		//------------------------------------------------------ 
		public Type Visit(If node) {
			//Console.WriteLine("Entro al IF");
			//Console.WriteLine("asdsfasfasdfsefs "+Visit((dynamic) node[0]));
			if (Visit((dynamic) node[0]) != Type.BOOL) {
					throw new SemanticError(
						"Expecting type " + Type.BOOL 
						+ " in conditional statement",                   
						node.AnchorToken);
			}
			foreach (var n in node) {
				if(n!= node[0]){
					VisitChildren(n);
				}
			}
		
			//Console.WriteLine("Termino IF");
			return Type.VOID;
		}

		//----------------------------------------------------------- 
		public Type Visit(Loop node) {
			isLoop.Push(true);
			VisitChildren (node);
			isLoop.Pop();	
			return Type.VOID;
		}

		//----------------------------------------------------------- Muy cabron x_x REVISAR
		public Type Visit(For node) {
			//Console.WriteLine ("Entre al FOR");
			isLoop.Push(true);
			var iter = node [0].AnchorToken.Lexeme;
			var oper = node [1].AnchorToken.Lexeme;
						//Console.WriteLine (iter + " - " +oper );
			Console.WriteLine(Table[node[1].AnchorToken.Lexeme][0]);
			if((Table[node[1].AnchorToken.Lexeme][0] != Type.LIST)){
				throw new SemanticError(
						"Expecting list in for statement",                 
						node.AnchorToken);
			}

			var tipo = Type.VOID;
			var tipo2 = Type.VOID;
			if(isProcDeclaration){

				if (AuxTable.Contains(iter)) {
						if( AuxTable[iter][0] == Type.LIST){
							tipo = AuxTable[iter][1];
						}else{
							tipo = AuxTable[iter][0];
						}
					}else if (Table.Contains (iter)) {
						if( Table[iter][0] == Type.LIST){
							tipo = Table[iter][1];
						}else{
							tipo = Table[iter][0];
						}				
					} else{
						throw new SemanticError (
							"Undeclared variable: ",
							node [0].AnchorToken);
					}
					//Console.WriteLine("Primero " + tipo);
					if (AuxTable.Contains (oper)) {
						//Console.WriteLine( AuxTable[oper][0]);

						if( AuxTable[oper][0] == Type.LIST){

							tipo2 = AuxTable[oper][1];
						}else{
							tipo2 = AuxTable[oper][0];
						}
					}else if (Table.Contains (oper)) {
						if( Table[oper][0] == Type.LIST){
							tipo2 = Table[oper][1];
						}else{
							tipo2 = Table[oper][0];
						}					
					} else{
						throw new SemanticError (
							"Undeclared variable: ",
						node [1].AnchorToken);
					}

			}else{
				if (Table.Contains (iter)) {
					if( Table[iter][0] == Type.LIST){
							tipo = Table[iter][1];
					}else{
							tipo = Table[iter][0];
					}	
				} else{
					throw new SemanticError (
						"Undeclared variable: ",
						node [0].AnchorToken);
				}
				if (Table.Contains (oper)) {
					if( Table[oper][0] == Type.LIST){
						tipo2 = Table[oper][1];
					}else{
						tipo2 = Table[oper][0];
					}							
				} else{
					throw new SemanticError (
						"Undeclared variable: ",
						node [1].AnchorToken);
				}
			}

	
			if(tipo != tipo2){
				throw new SemanticError (
					String.Format (
					"For requires two operands of type {1}",
					tipo),
					node.AnchorToken);
			}

			foreach(var n in node){
				if(n!=node[0]&&n!=node[1]){
					//Console.WriteLine(n);
					VisitChildren(n);
				}
			}
			//Console.WriteLine("Termina For");
			isLoop.Pop();
			return Type.VOID;
		}

		//----------------------------------------------------------- Revisar
		public Type Visit(Return node) {
			var x =0;
			

			foreach(var entry in node){
				x=1;
					if(Visit((dynamic)entry)!=Table[actualProc][1]){
					throw new SemanticError (
						"Expecting return type " + Table[actualProc][1] + " for proc " + actualProc,                   
						node.AnchorToken);
				}
			}
			
			if(Table[actualProc][1]!=Type.VOID&&x==0){
						throw new SemanticError (
						"Expecting return type " + Table[actualProc][1] + " for proc " + actualProc,                   
						node.AnchorToken);

			}
			return Type.VOID;
		}

		//------------------------------------------------------
		public Type Visit (Exit node) {
			if(isLoop.Count > 0){
				return Type.VOID;
			}
			 
			throw new SemanticError (
						"Can´t declare exit outside a loop or a for " + isLoop,                   
						node.AnchorToken);
		}

		//------------------------------------------------------
		public Type Visit (LogicOperator node) {
			var op = (string)node.AnchorToken.Lexeme;

			if (Visit ((dynamic)node [0]) != Type.BOOL && 
			    Visit ((dynamic)node [1]) != Type.BOOL) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires two operands of type {1}",
					op, Type.BOOL), node.AnchorToken);
			}

			return Type.BOOL;
		}

		//------------------------------------------------------
		public Type Visit (MulOperator node) {
			var op = (string)node.AnchorToken.Lexeme;
			if (Visit ((dynamic)node [0]) != Type.INT &&
			    Visit ((dynamic)node [1]) != Type.INT) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires two operands of type {1}",
					op, Type.INT), node.AnchorToken);
			}

			return Type.INT;
		}

		//------------------------------------------------------
		public Type Visit (SumOperator node) {
			var op = (string)node.AnchorToken.Lexeme;
			var n0 = Visit ((dynamic)node [0]);
			var n1 = Visit ((dynamic)node [1]);

			if ( !(n0 == Type.INT && n1 == Type.INT) ) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires two operands of type {1} but found: {2} {0} {3}",
					op, Type.INT, n0, n1), node.AnchorToken);
			}

			return Type.INT;
		}

		//------------------------------------------------------
		public Type Visit (Not node) {
			var op = (string)node.AnchorToken.Lexeme;
			var n0 = Visit ((dynamic)node [0]);

			if ( n0 != Type.BOOL ) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires operand of type {1} but found: {0} {2}",
					op, Type.BOOL, n0), node.AnchorToken);
			}

			return Type.BOOL;
		}

		//------------------------------------------------------
		public Type Visit (Minus node) {
			var op = (string)node.AnchorToken.Lexeme;
			var n0 = Visit ((dynamic)node [0]);

			if ( n0 != Type.INT ) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires operand of type {1} but found: {0} {2}",
					op, Type.BOOL, n0), node.AnchorToken);
			}

			return Type.INT;
		}

		//-----------------------------------------------------------
		public Type Visit (StringN node) {
			return Type.STRING;
		}

		//-----------------------------------------------------------
		public Type Visit (Int node) {
			var intStr = node.AnchorToken.Lexeme;

			try {
				Convert.ToInt32 (intStr);

			} catch (OverflowException) {
				throw new SemanticError (
					"Integer literal too large: " + intStr, 
					node.AnchorToken);
			}

			return Type.INT;
		}

		//-----------------------------------------------------------
		public Type Visit (True node) {
			return Type.BOOL;
		}

		//-----------------------------------------------------------
		public Type Visit (False node) {
			return Type.BOOL;
		}

		//-----------------------------------------------------------
		void VisitChildren (Node node) {
			foreach (var n in node) {
				Visit ((dynamic)n);
			}
		}

		//------------------------------------------------------
		void PrintTable () {	
			/*Console.WriteLine("\nTable");

			foreach (var entry in Table) {
				 var str ="";
				foreach(var bla in entry.Value){
					str+= (bla +" ");
				}
				Console.WriteLine ("\t[" + entry.Key + "\t\t" + str + "]");
			}*/
			foreach (var entry in AuxTable) {
				 var str ="";
				foreach(var bla in entry.Value){
					str+= (bla +" ");
				}
				Console.WriteLine ("\t[" + entry.Key + "\t\t" + str + "]");
			}
		}

		//-----------------------------------------------------------
		void VisitBinaryOperator (string op, Node node, Type type) {
			if (Visit ((dynamic)node [0]) != type || 
				Visit ((dynamic)node [1]) != type) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires two operands of type {1}",
					op, 
					type),
					node.AnchorToken);
			}
		}

		//-----------------------------------------------------------
		void checkLogicOperator (string op, Node node) {
			if (Visit ((dynamic)node [0]) != Type.BOOL && 
			    Visit ((dynamic)node [1]) != Type.BOOL) {
				throw new SemanticError (
					String.Format (
					"Operator {0} requires two operands of type {1}",
					op, 
					Type.BOOL),
					node.AnchorToken);
			}
		}


	}
}