/*
  Griffin compiler - This class performs the lexical analysis, 
  (a.k.a. scanning).
  Copyright (C) 2014 Sebastián Hidalgo, Alejandro García, Alejandro Flores
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Griffin {

    class Scanner {

        readonly string input;

        static readonly Regex regex = new Regex(
            @"                             
                (?<Assign>			:=       				)
			  | (?<BlockComment>	[(][*](.*\n[^*)])*[*][)])
			  | (?<BracketOpen>		[[]				     	)
			  | (?<BracketClose>	[]]     				)
			  | (?<Comma>			[,]     				)
			  | (?<Colon>			[:]     				)
			  | (?<CurlyOpen>		[{]				     	)
			  | (?<CurlyClose>		[}]     				)
			  | (?<Equal>			[=]      				)              
			  | (?<False>   		false      				)
			  | (?<Unequal>    		<>      				)
			  | (?<LessEqual>    	<=       				)
			  | (?<Greater>   		[>]      				)
			  | (?<GreaterEqual>   	>=      				)
              | (?<IntLiteral> 		\d+       				)
			  | (?<LineComment>		--.*      				)
              | (?<Less>    		[<]       				)
              | (?<Minus>        	[-]       				)
              | (?<Mul>        		[*]       				)
              | (?<Newline>    		\n        				)
              | (?<ParLeft>    		[(]       				)
              | (?<ParRight>   		[)]       				)
              | (?<Plus>       		[+]       				)
			  | (?<Semicolon>		[;]     				)
              | (?<True>       		true      				)
			  | (?<StrLiteral> 		[""""].*?([""""]{2}.*?)*[""""]|[""""].*?[""""])     # Must go before Identifier              
              | (?<WhiteSpace> 		\s        				)     # Must go anywhere after Newline.
              | (?<Identifier> 		([a-zA-Z]+[0-9]*[_]*)* 	)
              | (?<Other>					  				)     # Must be last: match any other character.
            ", 
            RegexOptions.IgnorePatternWhitespace 
                | RegexOptions.Compiled
                | RegexOptions.Multiline
            );

        static readonly IDictionary<string, TokenCategory> keywords =
            new Dictionary<string, TokenCategory>() {
				{"and", TokenCategory.AND},
				{"begin", TokenCategory.BEGIN},
				{"boolean", TokenCategory.BOOL},
				{"const", TokenCategory.CONST},
				{"div", TokenCategory.DIV},
				{"do", TokenCategory.DO},
				{"elseif", TokenCategory.ELSEIF},
				{"else", TokenCategory.ELSE},
				{"end", TokenCategory.END},
				{"exit", TokenCategory.EXIT},
				{"for", TokenCategory.FOR},
                {"if", TokenCategory.IF},
				{"in", TokenCategory.IN},
                {"integer", TokenCategory.INT},
				{"list", TokenCategory.LIST},
				{"of", TokenCategory.OF},
				{"loop", TokenCategory.LOOP},
				{"not", TokenCategory.NOT},
				{"or", TokenCategory.OR},
				{"procedure", TokenCategory.PROC},
				{"program", TokenCategory.PROGRAM},
				{"return", TokenCategory.RETURN},
				{"rem", TokenCategory.REM},
				{"sand", TokenCategory.SAND},
				{"sor", TokenCategory.SOR},
				{"string", TokenCategory.STRING},
	            {"then", TokenCategory.THEN},
				{"true", TokenCategory.TRUE},
				{"var", TokenCategory.VAR},
				{"xor", TokenCategory.XOR}
            };

        static readonly IDictionary<string, TokenCategory> nonKeywords =
            new Dictionary<string, TokenCategory>() {
                {"Assign", TokenCategory.ASSIGN},
				{"BracketOpen", TokenCategory.BRACKETS_OPEN},
				{"BracketClose", TokenCategory.BRACKETS_CLOSE},
				{"CurlyOpen", TokenCategory.CURLY_BRACE_OPEN},
				{"CurlyClose", TokenCategory.CURLY_BRACE_CLOSE},
				{"Comma", TokenCategory.COMMA},
				{"Colon", TokenCategory.COLON},
				{"Equal", TokenCategory.EQUAL},
                {"False", TokenCategory.FALSE},
				{"Greater", TokenCategory.GREATER},
				{"GreaterEqual", TokenCategory.GREATER_EQUAL},
				{"Identifier", TokenCategory.IDENTIFIER},
                {"IntLiteral", TokenCategory.INT_LITERAL},
                {"Less", TokenCategory.LESS},
				{"LessEqual", TokenCategory.LESS_EQUAL},
				{"Minus", TokenCategory.MINUS},
                {"Mul", TokenCategory.MUL},
                {"ParLeft", TokenCategory.PARENTHESIS_OPEN},
                {"ParRight", TokenCategory.PARENTHESIS_CLOSE},
                {"Plus", TokenCategory.PLUS},
				{"Semicolon", TokenCategory.SEMICOLON},
				{"StrLiteral", TokenCategory.STRING_LITERAL},
                {"True", TokenCategory.TRUE},
				{"Unequal", TokenCategory.UNEQUAL}
            };

        public Scanner(string input) {
            this.input = input;
        }

        public IEnumerable<Token> Start() {

            var row = 1;
            var columnStart = 0;

            Func<Match, TokenCategory, Token> newTok = (m, tc) =>
                new Token(m.Value, tc, row, m.Index - columnStart + 1);

            foreach (Match m in regex.Matches(input)) {

                if (m.Groups["Newline"].Length > 0) {

                    // Found a new line.
                    row = row + 1;
                    columnStart = m.Index + m.Length;

				} else if (m.Groups ["WhiteSpace"].Length > 0 
				           || m.Groups ["LineComment"].Length > 0) {

					// Skip white space and comments.

				} else if(m.Groups["BlockComment"].Length > 0){
					row += m.Value.Split('\n').Length - 1;

				} else if (m.Groups["Identifier"].Length > 0) {

                    if (keywords.ContainsKey(m.Value)) {

                        // Matched string is a Griffin keyword.
                        yield return newTok(m, keywords[m.Value]);                                               

                    } else { 

                        // Otherwise it's just a plain identifier.
                        yield return newTok(m, TokenCategory.IDENTIFIER);
                    }

                } else if (m.Groups["Other"].Length > 0) {

                    // Found an illegal character.
                    yield return newTok(m, TokenCategory.ILLEGAL_CHAR);

                } else {

                    // Match must be one of the non keywords.
                    foreach (var name in nonKeywords.Keys) {
                        if (m.Groups[name].Length > 0) {
                            yield return newTok(m, nonKeywords[name]);
                            break;
                        }
                    }
                }
            }

            yield return new Token(null, 
                                   TokenCategory.EOF, 
                                   row, 
                                   input.Length - columnStart + 1);
        }
    }
}
