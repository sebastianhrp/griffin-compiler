# README #
## Griffin Compiler ##
### Version 0.5 ###
-------------------------------
### What is it? ###

The Griffin Compiler is the final project for the Compilers 
course (Fall 2014) at Tecnológico de Monterrey, CEM. 


### Included in this release: ###

* Lexical analysis
* Syntactic analysis
* AST construction
* Semantic analysis
* CIL code generation

### Documentation ###

The documentation available as of the date of this release is
included in HTML format in the docs/manual/ directory.


### Installation ###

To build, at the terminal type:

 
```
#!c#

make
```


To run, type:

```
#!c#


 ./griffin.exe <file_name>

```


### Licensing ###

This program is free software. You may redistribute it under the terms of
the GNU General Public License version 3 or later. See LICENSE for 
details.    


### Contact ###

If you need help using The Griffin Compiler or want to report a bug, please
feel free to contact us:

* Alejandro Flores Ibarra      <[A01167361@itesm.mx](A01167361@itesm.mx)>
* Alejandro García Tovar       <[A01167827@itesm.mx](A01167827@itesm.mx)>
* Sebastián Hidalgo Rodríguez  <[A01167273@itesm.mx](A01167273@itesm.mx)>


-------------------------------------------------------------------------------
Copyright (C) 2014-2015 The Griffin Compiler. ITESM.