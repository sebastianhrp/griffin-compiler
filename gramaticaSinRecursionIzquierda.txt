

‹program› 	→ 	[ ¿const ‹constant-declaration›+ ]
				[ ¿var ‹variable-declaration›+ ]
				‹procedure-declaration›*
				¿program ‹statement›* ¿end ¿;

‹constant-declaration› 	→ 	‹identifier› ¿:= ‹literal› ¿;

‹variable-declaration› 	→ 	‹identifier› ( ¿, ‹identifier› )* ¿: ‹type› ¿;

‹literal› 	→ 	‹simple-literal› | ‹list›

‹simple-literal› 	→ 	‹integer-literal› | ‹string-literal› | ‹boolean-literal›

‹type› 	→ 	‹simple-type› | ‹list-type›

‹simple-type› 	→ 	¿integer | ¿string | ¿boolean

‹list-type› 	→ 	¿list ¿of ‹simple-type›

‹list› 	→ 	¿{ [ ‹simple-literal› ( ¿, ‹simple-literal› )* ] ¿}

‹procedure-declaration› 	→ 	¿procedure ‹identifier›
								¿( ‹parameter-declaration›* ¿) [ ¿: ‹type› ] ¿;
								[ ¿const ‹constant-declaration›+ ]
								[ ¿var ‹variable-declaration›+ ]
								¿begin ‹statement›* ¿end ¿;

‹parameter-declaration› 	→ 	‹identifier› ( ¿, ‹identifier› )* ¿: ‹type› ¿;

‹statement› 	→ 	‹assignment-statement› | ‹call-statement› | ‹if-statement› | ‹loop-statement› | ‹for-statement› | ‹return-statement› | ‹exit-statement›

‹assignment-statement› 	→ 	‹identifier› [ ¿[ ‹expression› ¿] ] ¿:= ‹expression› ¿;

‹call-statement› 	→ 	‹identifier› ¿( [ ‹expression› ( ¿, ‹expression› )* ] ¿) ¿;

‹if-statement› 	→ 	¿if ‹expression› ¿then ‹statement›*
					( ¿elseif ‹expression› ¿then ‹statement›* )*
					[ ¿else ‹statement›* ] ¿end ¿;

‹loop-statement› 	→ 	¿loop ‹statement›* ¿end ¿;

‹for-statement› 	→ 	¿for ‹identifier› ¿in ‹expression› ¿do
						‹statement›* ¿end ¿;

‹return-statement› 	→ 	¿return [ ‹expression› ] ¿;

‹exit-statement› 	→ 	¿exit ¿;

‹expression› 	→ 	‹logic-expression›

##### logicE = a    (log-op rel-ex) = alfa    rel-exp = b

#####‹logic-expression› 	→ 	‹logic-expression› ‹logic-operator› ‹relational-expression› | ‹relational-expression›

‹logic-expression› → ‹relational-expression› ‹logic-expression-aux›

‹logic-expression-aux› → epsilon | ‹logic-operator› ‹relational-expression› ‹logic-expression-aux›

‹logic-operator› 	→ 	¿and | ¿sand | ¿or | ¿sor | ¿xor

##### rel-exp = a  (rel-op sum-ex) = alfa		sum-exp = b

#####‹relational-expression› 	→ 	‹relational-expression› ‹relational-operator› ‹sum-expression› | ‹sum-expression›

‹relational-expression› 	→  ‹sum-expression› ‹relational-expression-aux›

‹relational-expression-aux› 	→ 	epsilon | ‹relational-operator› ‹sum-expression› ‹relational-expression-aux›

‹relational-operator› 	→ 	¿= | ¿<> | ¿< | ¿> | ¿<= | ¿>=

##### sum-ex = a    (sum-op mul-ex) = alfa     mul-exp = b

#####‹sum-expression› 	→ 	‹sum-expression› ‹sum-operator› ‹mul-expression› | ‹mul-expression›

‹sum-expression› 	→ 	‹mul-expression› ‹sum-expression-aux›

‹sum-expression-aux› 	→ 	epsilon | ‹sum-operator› ‹mul-expression› ‹sum-expression-aux›

‹sum-operator› 	→ 	¿+ | ¿-

##### mul-exp = a     (mul-op unary-exp) = alfa     unary-exp = beta

######‹mul-expression› 	→ 	‹mul-expression› ‹mul-operator› ‹unary-expression› | ‹unary-expression›

‹mul-expression› 	→ 	‹unary-expression› ‹mul-expression-aux›

‹mul-expression-aux› 	→ 	epsilon | mul-operator› ‹unary-expression› ‹mul-expression-aux›

‹mul-operator› 	→ 	¿* | ¿div | ¿rem

‹unary-expression› 	→ 	¿not ‹unary-expression› | ¿- ‹unary-expression› | ‹simple-expression›

‹simple-expression› 	→ 	( ¿( ‹expression› ¿) | ‹call› | ‹identifier› | ‹literal› )
							[ ¿[ ‹expression› ¿] ]

‹call› 	→ 	‹identifier› ¿( [ ‹expression› ( ¿, ‹expression› )* ] ¿)